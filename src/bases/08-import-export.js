// import { heroes } from './data/heroes';
// import { heroes } from './data/heroes'

// import heroes, { owners } from "../data/heroes";
import heroes from "../data/heroes";

const getHeroeById = (id) => heroes.find( ( heroe ) => heroe.id === id);

// console.log(getHeroeById(2));

const getHeroesByOwner = (owner) => heroes.filter( ( heroe ) => heroe.owner === owner);

export {
    getHeroeById,
    getHeroesByOwner
}
// console.log(getHeroesByOwner('DC'));
