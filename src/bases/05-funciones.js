


// Funciones en JS
// function saludar ( nombre ){
//    return `Hola, ${ nombre }`;
// } 

const saludar = function ( nombre ) {
    return `Hola, ${ nombre }`;
}

const saludar2 = ( nombre ) => `Hola, ${ nombre }`;

console.log(saludar2('Goku'));


const getUser = () =>  ({
        uid: 'ABC123',
        username: 'El_Papi'
});

const user = getUser();
console.log( user );

// Tarea
// 1. tranformar a funcion de flecha
// 2. retornar obj implicito
// 3. Prueba

const getUsuarioActivo  = ( nombre ) => ({
    uid: 'ABC567',
    username: nombre
})

const usuarioActivo = getUsuarioActivo( 'Aaron' );
console.log(usuarioActivo);

